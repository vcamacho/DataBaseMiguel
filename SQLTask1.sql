
/*************1. Para crear un Base de datos**************************/
Go
CREATE DATABASE SuperMercado
ON
	(	NAME=SuperMercado_dat,
		FILENAME='C:\BaseDatosSQL\SuperMercadodat.mdf',
		SIZE=10,
		MAXSIZE=50,
		FILEGROWTH=5)
LOG ON
	(	NAME=SuperMercado_log,
		FILENAME='C:\BaseDatosSQL\SuperMercadolog.ldf',
		SIZE=10,
		MAXSIZE=50,
		FILEGROWTH=5);
Go

/*************2. Para eliminar una Base de datos**********************/

USE master;
Go
DROP DATABASE SuperMercado;
Go

/*************3. Crear tablas en la base de datos**********************/

use SuperMercado;
Go
CREATE TABLE Clientes
	(	Id_Cliente int,
		First_Name char(50),
		Last_Name char(50),
		Address char(50),
		City char(50),
		Country char(50),
		Birth_Date datetime,
		Age int);
		

CREATE TABLE Productos
	(	Id_Producto int,
		Product_Name char(50),
		Product_Price decimal,
		Dead_Line datetime);

CREATE TABLE Ventas
	(
		Id_Cliente int,
		Id_Producto int,
		Total_Amount decimal);
Go

/*************4. Eliminar tablas en la base de datos**********************/
 
USE SuperMercado;
GO
DROP TABLE Clientes;
GO
/******************************************/

DROP TABLE SuperMercado.dbo.Productos;

DROP TABLE SuperMercado.dbo.Clientes;

DROP TABLE SuperMercado.dbo.Ventas;

/*************5. Modificar el nombre de la columna �Apellido� por �Apellidos� en la tabla  Clientes **********/
sp_rename 'Clientes.Last_Name', 'Last_NameS', 'COLUMN';

/*************6. Vaciar (eliminar todos los registros) de la tabla Ventas ********************* **********/
 
DELETE FROM Clientes

DELETE FROM Productos

DELETE FROM Ventas

/*************7. Insertar 5 registros en la tablas Clientes, Productos, y Ventas******************** **********/

INSERT INTO Clientes (Id_Cliente,First_Name, Last_NameS, Address, City, Country,Birth_Date,Age)
VALUES	('2','Thomas', 'Arandia Perez', 'Av. Blanco Galindo', 'Cochabamba', 'Bolivia', '12/08/1977', '41'),
		('3', 'Alejandro', 'Lopez Ledezma',  'Av. Villazon', 'Cochabamba', 'Bolivia', '10/24/1978', '42'),
		('4', 'Marco', 'Lopez Requena', 'Av. America', 'Cochabamba', 'Bolivia', '07/19/1945', '48'),
		('5', 'Pedro', 'Vera Quinteroz', 'Av. Circunvalacion', 'Cochabamba', 'Bolivia', '10/05/1942', '15');
		
		
		
INSERT INTO Productos(Id_Producto,Product_Name, Product_Price, Dead_Line)
VALUES	('1','Leche', '8', '08/19/2018'),
		('2', 'Chocolate', '20',  '08/30/2018'),
		('3', 'Coca Cola', '15', '08/15/2018'),
		('4', 'Pollo', '35', '08/20/2018');

		
INSERT INTO Ventas(Id_Cliente, Id_Producto, Total_Amount)
VALUES	('1','2','8'),
		('2', '1', '8'),
		('3', '4', '35'),
		('4', '1', '8');
		
/*************8. Listar todas las ventas ******************** **********/

SELECT *
FROM VENTAS 


SELECT CL.Id_Cliente, CL.First_Name, PR.Product_Name,PR.Product_Price,V.Total_Amount 
FROM CLIENTES CL, PRODUCTOS PR, VENTAS V

/*************9. Listar los primeros 10 clientes ******************** **********/

SELECT TOP 10 Id_Cliente, First_Name 
FROM CLIENTES 

/*************10. Listar los clientes agrupados por Edad******************** **********/

ORDENADOS POR Edad********************

SELECT Age, First_Name
FROM CLIENTES 
ORDER BY Age

CANTIDAD DE CLIENTES AGRUPADOS POR Edad********************

SELECT Age, count(*) as num_clientes
FROM CLIENTES 
GROUP BY Age

/*************11.  Listar los clientes cuyas edades sean mayores a 20 a�os******************** **********/

11. 
SELECT First_Name, Age
FROM CLIENTES 
WHERE Age>=20

/*************12. Listar los clientes cuyas edades sean mayores a 20 a�os******************************/

DELETE FROM CLIENTES 
WHERE Age>20














