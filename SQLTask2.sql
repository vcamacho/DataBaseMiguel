
/*****************************************************************/
/**************CREAR BASE DE DATOS********************************/
/*****************************************************************/
USE master;
Go
CREATE DATABASE Libreria
ON
	(	NAME=SuperMercado_dat,
		FILENAME='C:\BaseDatosSQL\Libreriadat.mdf',
		SIZE=10,
		MAXSIZE=50,
		FILEGROWTH=5)
LOG ON
	(	NAME=SuperMercado_log,
		FILENAME='C:\BaseDatosSQL\Librerialog.ldf',
		SIZE=10,
		MAXSIZE=50,
		FILEGROWTH=5);
Go
/*****************************************************************/
/************CREAR TABLAS*****************************************/
/*****************************************************************/

CREATE TABLE Persona(
	persona_id SMALLINT PRIMARY KEY NOT NULL,
	nombre VARCHAR(20) NOT NULL,
	apellido VARCHAR(20) NULL,
	genero CHAR(1) NOT NULL,
	fechaNacimiento DATE NULL,
	pais VARCHAR(20) NULL
);

CREATE TABLE Libro(
	libro_id SMALLINT NOT NULL,
	titulo VARCHAR(50) NOT NULL,
	precio DECIMAL(5,2) NULL,
	persona_id SMALLINT NOT NULL,
	PRIMARY KEY (libro_id),
	FOREIGN KEY(persona_id) REFERENCES Persona (persona_id)
);


/*****************************************************************/
/*******1. Registrar 7 personas con datos completos**************/ 
/****************************************************************/
INSERT INTO Persona 
VALUES (1, 'Jose', 'Perez',	'M', '1980-03-30', 'Bolivia');


INSERT INTO Persona 
VALUES	(2, 'Ana', 'Almeida',	'F', '1990-02-14', 'Brasil'),
		(3, 'Bill', 'Bush',	'M', '1970-02-14', 'EEUU'),
		(4, 'Jorge', 'Gonzales', 'M', '1995-10-21', 'Bolivia'),
        (5, 'Alejandro', 'Alvarez',	'M','1990-08-11', 'Colombia'),
		(6, 'Rodrigo', 'Lopez',	'M', '1990-05-05', 'Mexico'),
		(7, 'Marco', 'Gonzales', 'M', '1995-10-21', 'Colombia');	


/*****************************************************************/
/*******2. Registrar 3 personas sin nacionalidad*** **************/ 
/****************************************************************/
INSERT INTO Persona 
VALUES	(8, 'Angela', 'Arce', 'F','1990-08-21', ' '),
		(9, 'Veronica', 'Rocha', 'F', '1990-05-15', ' '),
		(10, 'April', 'Arandia', 'F', '1995-10-12', ' ');	


/**************************************************************************************/
/**********3. Registrar 1 persona sin nacionalidad ni fecha de nacimiento**************/ 
/**************************************************************************************/
VALUES	(11, 'Jammy', 'Montecinos', 'F', ' ', ' ');	


VALUES	(12, 'Pedro', 'Montecinos', 'M', NULL, NULL);	

INSERT INTO Persona 
(persona_id, nombre, genero, pais)
VALUES (13, 'Maria', 'F', 'Bolivia');


/*****************************************************************/
/**********4. Registrar 10 libros*********************************/ 
/****************************************************************/
INSERT INTO Libro 
VALUES	(2, 'Alborada', 50, 1);


INSERT INTO Libro 
VALUES	(2, 'Alborada', 50, 1),
		(3, 'Acostubrado a ganar', 57, 5),
		(4, 'Algoritmos', 40, 5),
		(5, 'SQL SERVER', 35, 8),
		(6, 'Serenidad', 70, 9),
		(7, 'Windows Server', 65, 2),
		(8, 'Calculo Numerico', 60, 2),
		(9, 'Algebra', 50, 9),
		(10, 'Trigonometria', 45, 3),
		(11, 'Electromecanica', 35, 1);


/********************************************************************************************/
/**********5.1 Listar el apellido de la tabla �Persona� ordenados por apellido **************/ 
/*******************************************************************************************/


SELECT nombre, apellido
FROM PERSONA
ORDER BY	apellido

/***************************************************************************************/
/**********5.2 Listar el nombre de la tabla �Persona� ordenados por nombre**************/ 
/***************************************************************************************/

SELECT nombre, apellido
FROM PERSONA
ORDER BY nombre

/*************************************************************************************************************************/
/**********6. Listar los libros con precio mayor a 50 Bs. y que el titulo del libro empieze con la letra �A�**************/ 
/*************************************************************************************************************************/

SELECT titulo, precio
FROM LIBRO
WHERE (precio>50) and (titulo LIKE'A%')


/*********************************************************************************************************************************/
/**********7. Listar el nombre y apellido de las personas que tengan al menos un libro cuyo precio sea mayor a 50 Bs**************/ 
/*********************************************************************************************************************************/

SELECT pe.nombre, pe.apellido, lb.titulo,lb.precio
FROM PERSONA pe INNER JOIN LIBRO lb ON pe.persona_id = lb.persona_id 
WHERE lb.precio>50


/***********************************************************************************************/
/**********8. Listar el nombre y apellido de las personas que nacieron el a�o 1990**************/ 
/***********************************************************************************************/

SELECT	nombre, apellido, fechaNacimiento
FROM	PERSONA
WHERE	fechaNacimiento like '1990%'



/********************************************************************************************************************/
/**********9. Listar los libros donde el titulo del libro contenga la letra �e� en la segunda posici�n**************/ 
/*******************************************************************************************************************/

SELECT *
FROM LIBRO
WHERE titulo LIKE '_E%'


/********************************************************************************/
/**********10. Contar el numero de registros de la tabla �Persona�**************/ 
/*******************************************************************************/

SELECT COUNT(*) as numregistros
FROM PERSONA

/*****************************************************************/
/**********11. Listar el o los libros m�s baratos*****************/  OJO
/****************************************************************/

SELECT MIN(precio)
FROM LIBRO

/*****************************************************************/
/**********12. Listar el o los libros m�s caros******************/ OJO
/****************************************************************/

SELECT MAX(precio)
FROM LIBRO


/*************************************************************************/
/**********13. Mostrar el promedio de precios de los libros**************/ 
/************************************************************************/

SELECT AVG(precio)  Promedio 
FROM LIBRO


/******************************************************************************/
/**********14. Mostrar la sumatoria de los precios de los libros**************/ 
/*****************************************************************************/
SELECT SUM(precio)
FROM LIBRO


/***********************************************************************************************************************/
/**********15. Modificar la longitud del campo titulo para que soporte hasta titulos de 300 caracteres******************/ 
/***********************************************************************************************************************/
ALTER TABLE LIBRO
ALTER COLUMN titulo VARCHAR(300)

/*****************************************************************************************/
/**********16. Listar las personas que sean del pa�s de Colombia � de Mexico**************/ 
/*****************************************************************************************/
SELECT *
FROM PERSONA
WHERE pais like 'Colombia' or pais like 'Mexico' 


SELECT * 
FROM PERSONA
WHERE pais IN ('Mexico', 'Colombia')

/********************************************************************************/
/**********17. Listar los libros que no empiezen con la letra �T� **************/ 
/*******************************************************************************/

SELECT *
FROM LIBRO
WHERE titulo like '[^t]%'

/*************************************************************************************/
/********** 18. Listar los libros que tengan un precio entre 50 a 70 Bs**************/ 
/************************************************************************************/
SELECT titulo, precio 
FROM LIBRO
WHERE precio BETWEEN 50 AND 70

/******************************************************************/
/**********19. Listar las personas AGRUPADOS por pa�s**************/ 
/*****************************************************************/

SELECT pais, nombre
FROM PERSONA
where pais is not NULL and pais <>'' 
GROUP BY pais, nombre

/**************************************************************************/
/**********20. Listar las personas AGRUPADOS por pa�s y genero**************/ 
/**************************************************************************/
SELECT pais, nombre, genero
FROM PERSONA
where pais is not NULL and pais <>'' 
GROUP BY pais, nombre, genero

/*************************************************************************************************/
/**********21. Listar las personas AGRUPADOS por genero, cuya cantidad sea mayor a 5**************/ 
/**************************************************************************************************/
SELECT genero, COUNT(Persona_id) as cantidad 
FROM persona 
GROUP BY genero 
HAVING COUNT(Persona_id)>5


/***************************************************************************************/
/**********22. Listar los libros ordenados por titulo en orden descendente**************/ 
/***************************************************************************************/

SELECT titulo
FROM LIBRO 
ORDER BY titulo ASC

/*************************************************************************/
/**********23. Eliminar el libro con el titulo �SQL Server�**************/ 
/************************************************************************/
DELETE FROM LIBRO
WHERE titulo='SQL SERVER'

/*********************************************************************************/
/**********24. Eliminar los libros que comiencen con la letra �A� ****************/ 
/*********************************************************************************/
DELETE FROM LIBRO
WHERE titulo LIKE'A%'

/*************************************************************************/
/**********25. Eliminar las personas que no tengan libros **************/ 
/*************************************************************************/
 DELETE FROM PERSONA 
 WHERE persona_id NOT IN 
				(SELECT DISTINCT persona_id 
				 FROM LIBRO)

/******************************************************************/
/**********26. Eliminar todos los libros   ************************/ 
/*****************************************************************/
DELETE FROM LIBRO

/*********************************************************************************************/
/**********27. Modificar el nombre de la persona llamada �Marco� por �Marcos�   **************/ 
/**********************************************************************************************/

UPDATE PERSONA 
SET nombre='MarcoS' 
WHERE nombre='Marco'


/***********************************************************************************************************************/
/**********28. Modificar el precio del todos los libros que empiezen con la palabra �Calculo�  a  57 Bs.  **************/ 
/**********************************************************************************************************************/

UPDATE LIBRO
SET precio=57  
WHERE titulo like 'Calculo%'


/****************************************************************************************************************/
/********** 29. Inventarse una consulta con GROUP BY  y HAVING  y explicar el resultado que retorna.*************/ 
/****************************************************************************************************************/
SELECT titulo, COUNT(libro_id_id) as cantidad 
FROM LIBROS
GROUP BY titulo
HAVING COUNT(libro_id)>10

